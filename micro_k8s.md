# Micro K8S


Objectif : pouvoir capitaliser sur les compéentes ph.ec

Objectif : pouvoir installer plus facilement plusieurs apps en parrallèle



Installé sur le serveur hetzner.

```
snap install microk8s --classic
```


Pour voir si le cluster est bien lancé :
```
microk8s.status
```


Pour activer les modules utiles :
```
microk8s.enable dns
microk8s.enable storage
```

Pour obtenir la config à utiliser via kubectl :
```
microk8s.config
```

A partir de là il est possible de déployer des applications kubernetes.

