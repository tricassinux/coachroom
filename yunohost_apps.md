# Applications Yunohost

## Nexcloud

Version 15.x disponible sous yunohost.
Cette version n'est plus supportée par Nextcloud.

Version actuelle de Nexcloud : 18 !!

Le [passage à la version 18](https://github.com/YunoHost-Apps/nextcloud_ynh/pull/253) sous yunohost [semble bloqué](https://github.com/YunoHost/issues/issues/1503).

Lors de l'installation de l'instance, cocher "Accéder au dossier personnel des utilisateurs depuis Nextcloud ?".


### Application Talk

Activer l'application **Talk**.


### Application Onlyoffice

Activer l'application **Onlyoffice**.

## Onlyoffice

Ajouter l'application yunohost **Onlyoffice**

## bitwarden

Obligation d'ajouter un domaine exclusif : `bitwarden.paxpar.tech`

Le config DNS proposé par yunohost est : 
```
; Basic ipv4/ipv6 records
@ 3600 IN A 51.15.225.178
* 3600 IN A 51.15.225.178
@ 3600 IN AAAA 2001:bc8:608:357::1
* 3600 IN AAAA 2001:bc8:608:357::1

; XMPP
_xmpp-client._tcp 3600 IN SRV 0 5 5222 bitwarden.paxpar.tech.
_xmpp-server._tcp 3600 IN SRV 0 5 5269 bitwarden.paxpar.tech.
muc 3600 IN CNAME @
pubsub 3600 IN CNAME @
vjud 3600 IN CNAME @

; Mail
@ 3600 IN MX 10 bitwarden.paxpar.tech.
@ 3600 IN TXT "v=spf1 a mx ip4:51.15.225.178 ip6:2001:bc8:608:357::1 -all"
mail._domainkey 3600 IN TXT "v=DKIM1; h=sha256; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC69t7rrVQedbqeXBtXuzfX5z3uPubRGxufhaXXeoeZmXU6CNHwbWtZOg27opVrY16ffapwsX8Eb0LdLN3b/P3hzqUylX4NTxJHwIzFXupuUuLKFWjgsSaLwp3kWytCmLIu14s03ZYV2UchP8rSk4+zUUCGFGGbfKfX7YyVSCERKQIDAQAB"
_dmarc 3600 IN TXT "v=DMARC1; p=none"

; Extra
@ 3600 IN CAA 128 issue "letsencrypt.org"
```

Comment grouper cette config avec celle de paxpar.tech ???
