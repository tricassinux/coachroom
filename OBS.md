# OBS

Objectif : un professeur peut *construire* sa vidéo en live comme un DJ
à partir de sa webcam, de sources vidéos, d'images, etc ...


Installation d'[OBS](https://obsproject.com/) sur ubuntu :
```
sudo apt install v4l-utils v4l2loopback-utils obs-studio v4l2loopback-dkms
```


Voir https://github.com/CatxFish/obs-virtual-cam/issues/17#issuecomment-487333181


Penser à modifier /dev/video1 en /dev/video2 si besoin :
```
sudo modprobe v4l2loopback devices=1 card_label="loopback 1" exclusive_caps=1,1,1,1,1,1,1,1
ffmpeg -re -f live_flv -i udp://localhost:12345 -f v4l2 /dev/video2
```


## variante

Voir https://srcco.de/posts/using-obs-studio-with-v4l2-for-google-hangouts-meet.html

```
sudo apt install obs-studio v4l2loopback-dkms
```

La caméra virtuelle sur /dev/video10 :
```
sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam" exclusive_caps=1
```

Plugin OBS : 
```
sudo apt install -y qtbase5-dev cmake libobs-dev
git clone --recursive https://github.com/obsproject/obs-studio.git
git clone https://github.com/CatxFish/obs-v4l2sink.git
cd obs-v4l2sink/build
cmake -DLIBOBS_INCLUDE_DIR="../../obs-studio/libobs" -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
sudo make install
```