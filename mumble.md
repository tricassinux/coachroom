

## Mumble 

Pour communiquer entre les utilisateurs de Tricassinux, nous utilisons le service Mumble, hébergé sur notre serveur.

Il faut installer un logiciel pour s'y connecter, il est disponible sur PC Windows, MacOS et Linux, ainsi que sur smartphone Android et iPhone.

### Matériel nécessaire 

Il faudra un appareil équipé d'un casque et d'un micro. Si possible il vaut mieux utiliser un casque qu'un haut-parleur, sinon les voix des autres utilisateurs en sortant par le  haut-parleur vont faire de l'écho dans le micro et cela va déranger tout le monde. 

Des écouteurs branchés sur un smartphone, et le micro intégré de ce dernier sont une bonne configuration.

### Conseils généraux 

 * Il peut être opportun de plutôt utiliser la version mobile de Mumble, qui permet si nécessaire de basculer entre une connexion wifi et une connexion 4G. L'installation et la configuration sont également plus aisées que sur ordinateur.

 * Si vous êtes connecté à Mumble et que vous n'êtes pas en cours de conversation ou prêt à communiquer, c'est mieux de penser à couper le micro et le casque (boutons "se rendre sourd" et "se rendre muet"), comme cela les autres utilisateurs savent que vous êtes sur autre chose, mais pas loin. 

 * Si lors d'une réunion votre matériel provoque des bruits parasites (par écho des autres utilisateurs ou à cause des bruits ambiants chez vous), il vaudra mieux configurer le logiciel Mumble pour activer votre voix uniquement lorsque vous appuyez sur une touche (voir plus bas à la partie "appuyez-pour-parler").

 * Pour ne pas être dérangé par la synthèse vocale, vous devriez la désactiver (voir configuration plus bas). Lorsque quelqu'un utilise la fonction discussion écrite (chat), cela indique normalement une notification sur votre ordinateur (mais pas sur smartphone)

 * Le logiciel Mumble fonctionne en arrière-plan, aussi vous pouvez utiliser un autre logiciel en même temps, par exemple un navigateur pour profiter d'un partage d'écran. Les touches de raccourcis définies dans Mumble (par exemple "appuyez-pour-parler") fonctionnent tant que Mumble est lancé, même s'il reste en arrière-plan.

 * Lorsque vous utilisez en même temps un autre service de partage d'écran ou de vidéo (jitsi par exemple), veillez à ce que dans ces derniers le son et/ou la vidéo sont bien coupés pour économiser la bande passante et éviter les larsens si le son est répété entre plusieurs logiciels.

 * Veuillez vérifier de temps en temps, lorsque vous parlez, que votre micro n'a pas été temporairement coupé par le SuperUser (si par exemple votre matériel fait des bruits parasites). Il n'y a que lui qui peut le réactiver.

 * Lors d'une réunion, veuillez rejoindre la "salle principale", où se trouvent les autres intervenants, pour être entendu par tous (le SuperUser peut vous déplacer automatiquement sinon).

 * Si la qualité de son n'est pas bonne, vous pouvez vous déconnecter et vérifier votre bande passante sur https://www.speedtest.net/ pour voir si le problème vient de chez vous. En dessous de 1-2 Mbps en descendant vous aurez du mal à télécharger et à entendre les autres, en dessous de 0.5 Mbps en ascendant les autres risquent d'avoir du mal à vous entendre, surtout si vous devez partager un écran ou de la vidéo en même temps. Mais normalement Mumble est bien optimisé en bande passante pour garantir une qualité de voix correcte.

### Configuration 

Quelque soit le mode d'utilisation, il faut entrer l'adresse du serveur dans la configuration.

 * L'adresse est toujours : tricassinux.org (sans https ni rien devant)
 * Laisser le port par défaut (64738)

### Installation sur Android 

 * La version Android se trouve ici : https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient.free

Ou chercher le logiciel "Plumble free" dans le Google Play Store.

![](images/mumble09.png) 

### Installation sur iPhone 

 * La version iOS (non testée) se trouve ici : https://apps.apple.com/us/app/mumble/id443472808

### Installation sur ordinateur 

 * Les versions ordinateurs se trouvent ici : https://www.mumble.com/mumble-download.php

 * Télécharger la version adaptée à votre PC :

![](images/mumble01.png) 

 * Laisser les paramètres d'installation par défaut :

![](images/mumble02.png) 

 * Passer l'assistant de configuration, vous pouvez tout laisser par défaut, ou essayer d'ajuster au mieux selon votre matériel :

![](images/mumble03.png) 

 * Il y a une création de certificat pour la connexion (à faire une seule fois), laisser les paramètres par défaut : 

![](images/mumble04.png) 

 * Ensuite, entrer juste ces informations : tricassinux.org comme Adresse, et laisser le port par défaut (64738).

Choisir un nom d'utilisateur à votre convenance (pas besoin de s'enregistrer) :

![](images/mumble05.png) 

 * Vous êtes maintenant connecté. Vous pouvez double-cliquer sur la salle "Salle de réunion principale" pour retrouver les autres utilisateurs. Si vous avez besoin de discuter en privé avec une partie du groupe, vous pouvez vous retrouver dans une des salles secondaire :

![](images/mumble10.png) 

### Configurations avancées 

 * En faisant un clic droit sur votre nom, vous pouvez enregistrer le certificat de votre ordinateur sur le serveur, c'est facultatif, mais permet ensuite de créer des salles temporaires :

![](images/mumble07.png) 

 * Il est conseillé de désactiver la synthèse vocale qui ne sert pas à grand chose :

![](images/mumble08.png) 

 * Il est possible également de configurer Mumble pour activer le "appuyez-pour-parler", qui ne déclenche la voix que lorsque vous appuyez sur une touche du clavier, et qui coupe la voix lorsque vous relâchez cette touche (à définir plus loin dans les préférences) :

![](images/mumble11.png) 

 * Les touches de raccourcis clavier sont à définir dans ce menu :

![](images/mumble13.png) 

Vous pouvez indiquer ici quelle touche affecter au "appuyez-pour-parler", si vous l'avez activé dans le menu précédent. Vous pouvez également à la place laisser le micro en détection automatique et définir une autre touche du clavier pour couper ou réactiver le micro. 

