# Installation serveur Hertzner

Installation en indiquant nue clé publique SSH.


Mail de confirmation avec les adresses IP :
```
Dear Mr Philippe ENTZMANN

Thank you for your order B20200321-1506702 from 21/03/2020 10:22 CET.

Login Details
The Linux installation for your server EX42-NVMe #1159644 (88.99.149.232) is complete. You can now access the server via SSH2 using the following details:

IPv4 Address:	88.99.149.232
IPv6 Address:	2a01:4f8:10a:2e27::2
Username:	root
Public key:	philippe@deskmini b0:9b:29:f3:84:92:76:8d:9a:35:aa:f0:88:aa:83:57 (RSA 4096)
Host key:	82:b2:08:26:4f:2a:b1:ad:d5:83:7a:27:68:23:57:25 (RSA 2048)
e8:7f:07:5f:3a:d1:dc:e7:10:a6:9f:90:c0:19:42:88 (ECDSA 256)
82:e2:6f:c6:2b:85:aa:63:4d:63:b7:f6:67:c8:4b:41 (ED25519 256)
Downloads
We have collected a number of packages, tools and OS images and offer them as downloads via our mirror. These files can be found at https://download.hetzner.de, using the following login credentials:

Username:	hetzner
Password:	download
Guides
A Quick Start Guide for Dedicated Root Servers with helpful information can be found in our wiki:
http://wiki.hetzner.de/index.php/Root_Server_Guide/en.

Support
In order to process your support enquiries as quickly and efficiently as possible, please send your requests directly via the Robot administration interface. Click on the user icon in the upper right hand corner and then on "Support".

Our support team is also available to answer your queries via the telephone:

Main Office:	Monday - Friday:	7:30 am - 6:00 pm
+49 9831 505-0
Our data centre technicians are available during the following business hours:
Monday - Sunday:	24 hours
Data center park Nuremberg (NBG1):		+49 911 234226-54
Data center park Falkenstein (FSN1):		+49 374 574447-100
Data center park Helsinki (HEL1):		+358 753259-100
When contacting our technicians in the DC directly a phone password is required to authenticate you as the owner of the server, allowing our technicians to perform some tasks on the server. A phone password can be set in the Robot, under the "Phone Password" tab of the server.

Fault reports and announcements can be found on the Hetzner Status Web Page:
https://www.hetzner-status.de/en.html.

Kind regards

Your Hetzner Online Team
```

## post-install

```
apt update
apt install -y byobu
```

Pour connaître le nom du compte *admin* :
```
sudo -u www-data php /var/www/nextcloud/occ user:list
```

Pour changer le mot de passe du compte :
```
sudo -u www-data php /var/www/nextcloud/occ user:resetpassword root
```

Activation du pack d'application : Hub bundle (contenant Talk !)


TODO: configurer pour utiliser Onlyoffice !

