# Utilisation de Git et de Gitlab 

Ce document donne quelques instructions sur la gestion de code ou de projet, grâce au logiciel Git et à l'hébergeur Gitlab.


## Installations 

- Installer notepad++ : https://notepad-plus-plus.org/downloads/ 

- Installer TortoiseGit : https://tortoisegit.org/download/ (c'est probablement la version windows 64 bit qu'il faut, si ça ne fonctionne pas la première fois, choisir ensuite la version 32 bit si votre version de windows est finalement en 32 bit)

 - dans les options d'installation :
	- Choisir TortoiseGitPlink
	
	
- Installer Git : https://git-scm.com/download/win

 - Laisser par défaut :
	- Select compoments
 - Modifier : 
	- choisir d'utiliser notepad++ et non pas vim (qui est plus compliqué)

 - Ensuite laisser par défaut : 
	- choisir "git from the command line and also from 3rd-party software"
 - Modifier : 
	- use (Tortoise)Plink (il devrait l'avoir détecté de l'installation précédente)
 - Ensuite laisser par défaut : 
	- use the openssl library
	- checkout windows-style
	- use MinTTY
	- Configuring extra options : laisser tout par défault (je ne sais pas si c'est nécessaire "enable symbolic links" mais on va essayer de faire sans)



## Utilisation 

### Initialiser un dépôt 

- Pour initialiser un dépôt existant sur internet, aller dans l'explorateur de documents où vous souhaitez ajouter votre projet, faire un clic-droit et choisir "git clone". Entrez l'URL du projet (sur github ou gitlab)

![](images/tortoisegit01.png)

- Entrez vos identifiants si le projet n'est pas public :

![](images/tortoisegit02.png)


Cela va récupérer les fichiers du projet.

Modifiez le code comme vous souhaitez.


### Faire un commit 

Ensuite, clic droit dans le dossier de travail (ou sous-dossier, c'est pareil), et choisir "Git commit -> master" 


![](images/tortoisegit06.png)

La première fois cela va indiquer "User name and email must be set before commit". Entrez le nom : pas obligatoirement celui dans github/gitlab, c'est pour tracer vos modifications, ce n'est pas un login. Mais c'est mieux si ça peut être consistant avec le reste du projet.

Ensuite vous pouvez indiquer les modifications apportées (rajout de fonctionnalité dans le code, modification de bug etc). Vous pouvez également seulement indiquer "maj", "ras", wtf" ou "--" à la place.

Si vous avez ajouté de nouveaux fichiers dans le projet (images, code ou autre), il y a une case à cocher dans la partie "not versioned files". Vous pouvez décider de rajouter ici ces fichiers.

![](images/tortoisegit03.png)


Mais attention, ne rajoutez ici que ce qui est nécessaire au projet ! Les binaires ou des fichiers temporaires générés par votre code pourront être détectés ici comme nouveau fichiers, il ne faut pas les ajouter, cela va encombrer le dépôt et les transferter pour rien. Inform7 en particulier génère de nombreux fichiers html et temporaires qui ne servent pas pour le code.


### Faire un push 

Une fois le commit réalisé, profitez-en pour appuyer sur "push" :


![](images/tortoisegit04.png)

Le commit est toujours en local. Le push sert pour envoyer le ou les derniers commit sur le dépôt distant.
Il n'est pas nécessaire de toujours réaliser les push, mais il est indispensable de le faire à la fin d'une session de travail. 

Pour cela, la commande de push est en faisant un clic-droit, "TortoiseGit > Push"


Il n'y a pas de règle spécifique, mais les commits servent à indiquer des points dans le développement. Si vous modifiez du code et que vous voulez revenir facilement à un état antérieur, créer un commit juste avant facilitera le retour en arrière. On peut voir ça comme un "point de sauvegarde". 

On peut bien entendu faire un push après chaque commit (c'est ce qui arrive si on modifie du code depuis l'interface web de gitlab ou de github).


Pour le push on laisse tout par défaut et on appuie sur "ok". On ne va pas travailler sur des branches qui compliquent le travail (c'est réservé pour des développements spécifiques dans des logiciels complexes).

Important : Avant une session de travail, il faut toujours vérifier si les autres utilisateurs ont fait des modifications de leur côté, et récupérer ce travail si nécessaire. Clic-droit dans un dossier du projet, et choisir "tortoiseGit > Pull". Laisser les options par défaut, faire "ok".

S'il y a eu des fichiers modifiés, cela les indiquera. Sinon ça écrira dans la console : "Already up to date."

![](images/tortoisegit05.png)


Vous pouvez également dans le dossier de travail implicitement rajouter des fichiers dans le dépôt, c'est à dire que vous avez copié ces fichiers depuis l'explorateur de document, et pour que les autres utilisateurs du projet les récupèrent également, vous faites clic-droit "TortoiseGit > add". Mais le plus simple est de les rajouter lors d'un commit puisque les nouveaux fichiers seront détectés.




## Problèmes divers


- Si à la suite d'un push vous obtenez le message :

```
! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'https://gitlab.com/eforgeot/les_mille_routes.git'

hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
``` 

![](images/tortoisegit07.png)

C'est parce qu'il y a eu des changements dans le dépôt par un autre développeur en même temps que vous travailliez. 

Il faut donc faire tout d'abord un "pull" (voir plus haut, mais à la suite d'un tel message d'erreur on peut le faire tout de suite dans l'interface), et une fois ceci fait, si tout c'est bien passé, refaire un push ensuite.


- Si un même fichier que vous avez modifié a été également modifié par un autre développeur, si c'est sur des parties de fichier différentes, ça devrait se mélanger sans trop de problème :


![](images/tortoisegit08.png)


(vous pouvez voir un "diff" pour la différence de texte)


